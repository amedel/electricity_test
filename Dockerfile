FROM alpine/git as clone
WORKDIR /app
RUN git clone https://gitlab.com/amedel/electricity_test.git

FROM maven:3.5-jdk-8-alpine as build
WORKDIR /app
COPY --from=clone /app/electricity_test /app
RUN mvn install

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build /app/target/electricityTest-0.0.1.jar /app
EXPOSE 8080
ENTRYPOINT ["sh", "-c"]
CMD ["java -jar electricityTest-0.0.1.jar"] 