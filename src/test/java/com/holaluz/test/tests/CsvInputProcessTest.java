package com.holaluz.test.tests;

import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import com.holaluz.test.processor.impl.CsvInputProcessor;

public class CsvInputProcessTest {
	
	@Test
	public void processTest() {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream is = classloader.getResourceAsStream("test.csv");
		CsvInputProcessor csvInputProcessor = new CsvInputProcessor();
		
		try {
			String suspicious = csvInputProcessor.process(is);
			Assert.assertNotNull(suspicious);
			Assert.assertFalse(suspicious.isEmpty());
		} catch (Exception e) {
			Assert.assertTrue(e.getMessage(), false);
		}
	}
	
	
}
