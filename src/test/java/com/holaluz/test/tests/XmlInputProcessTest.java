package com.holaluz.test.tests;

import java.io.InputStream;

import org.junit.Assert;
import org.junit.Test;

import com.holaluz.test.processor.impl.XmlInputProcessor;

public class XmlInputProcessTest {

	@Test
	public void processTest() {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("test.xml");
			XmlInputProcessor xmlInputProcessor = new XmlInputProcessor();
			
			try {
				String suspicious = xmlInputProcessor.process(is);
				Assert.assertNotNull(suspicious);
				Assert.assertFalse(suspicious.isEmpty());
			} catch (Exception e) {
				Assert.assertTrue(e.getMessage(), false);
			}
			
	}
	
}
