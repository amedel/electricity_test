package com.holaluz.test.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.holaluz.test.generator.StaticsGenerator;

@Controller
@ComponentScan("com.holaluz")
@EnableAutoConfiguration
public class ElectricityTestController {
	
	private Logger logger = Logger.getLogger(ElectricityTestController.class.getCanonicalName());
	
	@Autowired
	StaticsGenerator staticsGenerator;
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	@PostMapping("/statics/from/file")
	@ResponseBody
	public String getStatics(@RequestParam("fileInput") MultipartFile file) {
		String jsonStr="";
		Map<String, Object> model = new HashMap<>();
		try {
			jsonStr = staticsGenerator.generate(file.getInputStream(),file.getOriginalFilename());
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Reading file from front", e);
		}
		logger.log(Level.FINE,"Answer json" + jsonStr);
		model.put("json", jsonStr);
		return jsonStr;//new ModelAndView("index", model);
	}

}
