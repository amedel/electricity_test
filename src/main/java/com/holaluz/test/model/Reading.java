package com.holaluz.test.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Reading implements Comparable<Reading> {

	private String clientId;
	private LocalDate period;
	private Long amount;
	private Double median;
	
	
	public Reading(String clientId, LocalDate period, Long amount) {
		this();
		this.clientId = clientId;
		this.period = period;
		this.amount = amount;
	}

	public Reading() {
		super();
	}
	
	
	@Override
	public int compareTo(Reading other) {
		return this.amount.compareTo(other.getAmount());
	}

	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonFormat (pattern="yyyy-MM")
	public LocalDate getPeriod() {
		return period;
	}


	public void setPeriod(LocalDate period) {
		this.period = period;
	}


	public Long getAmount() {
		return amount;
	}


	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Double getMedian() {
		return median;
	}

	public void setMedian(Double median) {
		this.median = median;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reading other = (Reading) obj;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reading [clientId=" + clientId + ", period=" + period + ", amount=" + amount + ", median=" + median
				+ "]";
	}

	
}
