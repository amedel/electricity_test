package com.holaluz.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.holaluz.test.controller.ElectricityTestController;

@SpringBootApplication

public class ElectricityTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElectricityTestController.class, args);
	}
}