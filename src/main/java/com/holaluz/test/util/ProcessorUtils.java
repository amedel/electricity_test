package com.holaluz.test.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.holaluz.test.model.Reading;

public class ProcessorUtils {
	
	public static final String CLIENT_ID = "clientID";
	public static final String PERIOD = "period";
	public static final String NODE_NAME = "reading";
	public static final String CSV_CHARACTER_SEPARATOR = ",";
	
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	static {
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}
	

	public static List<Reading> getSuspicious(final List<Reading> readingList) {
		Map<String,Double> clientMedianMap;
		List<Reading> suspiciousReads = new ArrayList<>();
		
		clientMedianMap = getClientsMedian(readingList);
		
		clientMedianMap.keySet().stream().forEach(
				clientId -> {
					double clientMedian = clientMedianMap.get(clientId);
					readingList.stream().filter(read->{
						double clientRead = read.getAmount().doubleValue();
						boolean isSuspicious = false;
						if(read.getClientId().equals(clientId) && ((clientRead < clientMedian*0.5) || (clientRead>clientMedian*1.5))){
							isSuspicious=true;
						}
						return isSuspicious;
					}).forEach(suspiciousRead -> {
							suspiciousRead.setMedian(clientMedian);
							suspiciousReads.add(suspiciousRead);
						});
				});

		return suspiciousReads;
		
	}
	
	public static Map<String, Double> getClientsMedian(final List<Reading> readingList) {
		Map<String,Double> clientMedianMap = new HashMap<>();
		
		readingList.stream().distinct().forEach(
				uniqueReading->{
					double clientMedian = readingList.stream().filter(
											read -> {
												return uniqueReading.getClientId().equals(read.getClientId());
											}).sorted()
											  .sequential()
											  .skip(5)
											  .limit(2)
											  .mapToLong(readingAmount -> {return readingAmount.getAmount();})
											  .sum();
					clientMedianMap.put(uniqueReading.getClientId(),clientMedian/2);
				}
			);
		
		return clientMedianMap;
	}
	
	
	public static String suspiciousListToJson(final List<Reading> suspiciousList) throws JsonProcessingException {
		String returnJson = objectMapper.writeValueAsString(suspiciousList);
		
		return returnJson;
	}
	
}
