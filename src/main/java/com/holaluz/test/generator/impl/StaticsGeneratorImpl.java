package com.holaluz.test.generator.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.holaluz.test.generator.StaticsGenerator;
import com.holaluz.test.processor.InputProcessor;

@Component
public class StaticsGeneratorImpl implements StaticsGenerator {

	private Logger logger = Logger.getLogger(StaticsGeneratorImpl.class.getCanonicalName());

	
	@Autowired
	@Qualifier(value="xmlInputProcessor")
	InputProcessor xmlInputProcessor;
	
	@Autowired
	@Qualifier(value="csvInputProcessor")
	InputProcessor csvInputProcessor;
	
	@Override
	public String generate(InputStream inputStream, String fileName) {
		String returningJson;
		try {
			String extension = fileName.substring(fileName.length()-3);
			
			logger.log(Level.FINE,"extension:  " + extension);
			
			if(extension.contains("xml")) {
				returningJson = xmlInputProcessor.process(inputStream);
				
			}else if(extension.contains("csv")) {
				returningJson = csvInputProcessor.process(inputStream);
				
			}else {
				throw new IllegalArgumentException("Error identifying file type");
			}
		}catch(IOException e) {
			returningJson ="{\"error\":\""+e.getMessage()+"\"}";
			logger.log(Level.SEVERE,"reading input", e);
		} catch (Exception e) {
			returningJson ="{\"error\":\""+e.getMessage()+"\"}";
			logger.log(Level.SEVERE, "unknown", e);
		}
			
		return returningJson;	
	}

}
