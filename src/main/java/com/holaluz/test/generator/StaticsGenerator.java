package com.holaluz.test.generator;

import java.io.InputStream;

public interface StaticsGenerator {

	public String generate(InputStream inputStream, String fileName);
}
