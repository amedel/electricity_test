package com.holaluz.test.processor.impl;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.holaluz.test.model.Reading;
import com.holaluz.test.processor.InputProcessor;
import com.holaluz.test.util.ProcessorUtils;

@Component
public class XmlInputProcessor implements InputProcessor {

	private Logger logger = Logger.getLogger(XmlInputProcessor.class.getCanonicalName());
	
	@Override
	public String process(InputStream input) throws Exception {
		
		Node node;
		Reading reading;
		Element element;
		String clientId;
		String returnJson;
		LocalDate period;
		Long amount;
		List<Reading> readingList = new ArrayList<>();
		List<Reading> suspiciousList;
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(input);
		doc.getDocumentElement().normalize();
		
		logger.log(Level.FINE, "Root element :" + doc.getDocumentElement().getNodeName());
		NodeList nList = doc.getElementsByTagName(ProcessorUtils.NODE_NAME);
		
		for(int idx=0; idx < nList.getLength();idx++) {
			node = nList.item(idx);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				element = (Element) node;
				clientId = element.getAttribute(ProcessorUtils.CLIENT_ID);
				period = LocalDate.parse(element.getAttribute(ProcessorUtils.PERIOD)+"-01");
				amount = Long.valueOf(element.getTextContent());
				logger.log(Level.FINE,"element :" + doc.getDocumentElement().getAttributes());
				reading = new Reading(clientId, period, amount);
				logger.log(Level.FINE,"Parsed element :" + reading);
				readingList.add(reading);
			}
		}
		suspiciousList = ProcessorUtils.getSuspicious(readingList);
		returnJson = ProcessorUtils.suspiciousListToJson(suspiciousList);
		logger.log(Level.FINE,"Json resultado: " + returnJson);
		
		return returnJson;
	}

}
