package com.holaluz.test.processor.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.holaluz.test.model.Reading;
import com.holaluz.test.processor.InputProcessor;
import com.holaluz.test.util.ProcessorUtils;

@Component
public class CsvInputProcessor implements InputProcessor {

	
	private Logger logger = Logger.getLogger(CsvInputProcessor.class.getCanonicalName());
	
	
	@Override
	public String process(InputStream input) throws Exception {
		
		Reading reading;
		String clientId;
		String returnJson;
		LocalDate period;
		Long amount;
		String[] lineAttributes;
		List<Reading> suspiciousList;
		List<Reading> readingList = new ArrayList<>();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input));
		
		String line = bufferedReader.readLine(); 
		
		while(line!=null) {
			if(line.contains("client,")) {
				line = bufferedReader.readLine();
			}else {
				lineAttributes = line.split(ProcessorUtils.CSV_CHARACTER_SEPARATOR);
				if(lineAttributes.length==3) {
					clientId = lineAttributes[0];
					period = LocalDate.parse(lineAttributes[1]+"-01");
					amount = Long.valueOf(lineAttributes[2]);
					reading = new Reading(clientId,period,amount);
					logger.log(Level.FINE,"Parsed element :" + reading);
					readingList.add(reading);
				}
				line = bufferedReader.readLine();
			}
		}
		
		suspiciousList = ProcessorUtils.getSuspicious(readingList);
		returnJson = ProcessorUtils.suspiciousListToJson(suspiciousList);
		logger.log(Level.FINE,"Json resultado: " + returnJson);	
		
		return returnJson;
	}

}
