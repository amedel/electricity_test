package com.holaluz.test.processor;

import java.io.InputStream;

public interface InputProcessor {

	public String process(InputStream input)throws Exception;
	
}
