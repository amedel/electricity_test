
jQuery(document).ready(function(){

	jQuery('#suspiciousTable').hide();
	jQuery('#errorTable').hide();
	
	var fillTable = function(data){
		jQuery('#suspiciousTable').hide();
		jQuery('#errorTable').hide();
		var jsonData = JSON.parse(data);
		var tableBody = jQuery('#resultTable tbody');
		var errorTableBody = jQuery('#errorResultTable tbody');
		tableBody.empty();
		errorTableBody.empty();
		if(jsonData){
			if(jsonData['error']){
				var tableBodyRow = jQuery('<tr id="readingError">');
				tableBodyRow.append(jQuery('<td id="errorCause">').text(jsonData['error']));
				errorTableBody.append(tableBodyRow);
				jQuery('#errorTable').show();
			}else{
				for(idx in jsonData){
					var reading = jsonData[idx];
					
					var tableBodyRow = jQuery('<tr id="reading'+idx+'">');
					if(idx%2!=0){
						tableBodyRow.addClass('alt');
					}
					tableBodyRow.append(jQuery('<td id="client'+idx+'">').text(reading['clientId']));
					tableBodyRow.append(jQuery('<td id="period'+idx+'">').text(reading['period']));
					tableBodyRow.append(jQuery('<td id="suspicious'+idx+'">').text(reading['amount']));
					tableBodyRow.append(jQuery('<td id="median'+idx+'">').text(reading['median']));
					tableBody.append(tableBodyRow);
				}
				jQuery('#suspiciousTable').show();
			}
		}
		
	};

	$("#fileForm").submit(function(event){
	 
	  event.preventDefault();
	 
	  //grab all form data  
	  var formData = new FormData($(this)[0]);
	 
	  $.ajax({
	    url: '/statics/from/file',
	    type: 'POST',
	    data: formData,
	    async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function (returndata) {
	      fillTable(returndata);
	    },
	  });
	 
	  return false;
	});



});